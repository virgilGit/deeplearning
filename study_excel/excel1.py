import pandas as pd
import numpy as np
import itertools
import pickle

# 类对象


class AnalysisEmp:
    # 已休息天数
    restDays = 0
    # 连续工作天数
    workingDays = 0
    # 早班工作天数
    mornWorkingDays = 0
    # 晚班工作天数
    nightWorkingDays = 0
    # 1618工作天数
    officeWorkingDays = 0
    # 昨天是否晚班 0 否; 1 是
    isLastDayNight = 0
    # 早班已经排了人 0 否；1 是
    mornWorked = 0
    # 晚班已经排了人 0 否；1 是
    nightWorked = 0
    # 1618已排了人
    officeWorked = 0
    # 休息已经排了人 0 否；1 是
    rested = 0
    # 周几
    weekNum = "一"
    # 本次循环，标记本次的班次类型 休 早班 晚班 1618
    workType = ""

    def castToArray(self):
        return [
            self.restDays,
            self.workingDays,
            self.mornWorkingDays,
            self.nightWorkingDays,
            self.officeWorkingDays,
            self.isLastDayNight,
            self.mornWorked,
            self.nightWorked,
            self.officeWorked,
            self.rested,
            self.findInDictionary(self.weekNum, self.chinese_to_arabic)
        ]
    
    # 转换weekType 2 one-hot 格式
    workType2OneHot = {
        "早班": [1,0,0,0],
        "晚班": [0,1,0,0],
        "1618": [0,0,1,0],
        1618: [0,0,1,0],
        "休": [0,0,0,1],
        "大": [0,0,0,1]
    }

    # 定义中文数字到阿拉伯数字的映射
    chinese_to_arabic = {
        "一": 1,
        "二": 2,
        "三": 3,
        "四": 4,
        "五": 5,
        "六": 6,
        "日": 7
    }

    # 定义一个函数来转换中文数字
    def findInDictionary(self, key, dict):
        # 检查中文字符是否在映射中
        if key in dict:
            return dict[key]
        else:
            return None  # 或者可以抛出一个异常
    
    # 输出one-hot 
    def cast2OneHotData(self):
        return self.findInDictionary(self.workType, self.workType2OneHot)

# employeePreList 是上个月遗留数据
# [
#   [xxxx]
#   [xxxx]
#   [xxxx]
#    ]
# TODO 要考虑到员工个数不确定
def load_excel(excel_path, employeePreList=None, employeesPre=None):

    # 读取excel
    df = pd.read_excel(excel_path, engine='openpyxl')
    # print(df.head())
    # 将DataFrame转换为二维数组
    array = df.values
    print(array.shape[0])
    print(array.shape[1])

    # 保存姓名信息
    # employees = ["殷庆涛", "陈玉龙", "李阳"]
    employees = []

    # 还没确定下面这个对象二维表
    employeeList = [[AnalysisEmp() for _ in range(array.shape[1])]
                    for _ in range(array.shape[0])]  # 这里array.shape[0] - 2
    print(f"employeeList的形状{len(employeeList)}")

    # 准备给下一个excel的前置employeePreList
    forNextPreEmployeeList = []

    for i in range(array.shape[1]):
        mornWorkedFlag = 0  # 这一天早班已经排人了
        nightWorkedFlag = 0  # 这一天晚班已经排人了
        officeWorkedFlag = 0  # 这一天1618以及排人了
        restedFlag = 0  # 休息已经排人了
        day = 0
        weekDay = ''
        for j in range(array.shape[0]): # 行数代表了：day + week + employeeNum

            employee = employeeList[j][i]  # 找到第几个员工信息
            employeePreDay = None  # 找到该员工前一天的记录
            element = array[j][i]
            if j == 0:
                day = element
            elif j == 1:
                weekDay = element
                print(weekDay)
            else:
                if (i > 1):
                    # 获取前一个employee信息
                    employeePreDay = employeeList[j][i - 1]
                    # 遗传数据
                    employee.restDays = employeePreDay.restDays
                    employee.workingDays = employeePreDay.workingDays
                    employee.mornWorkingDays = employeePreDay.mornWorkingDays
                    employee.nightWorkingDays = employeePreDay.nightWorkingDays
                    employee.officeWorkingDays = employeePreDay.officeWorkingDays
                elif (i == 1):
                    # TODO 这里还要思考
                    if (employeePreList is not None) and (employeesPre is not None) :
                        # j - 2，2分别是日期和星期
                        # emploees还没有确定 TODO 待测试
                        try:
                            employeePreDay = employeePreList[employeesPre.index(employees[j - 2])]
                            # 跨月抹除部分信息：休息日刷新
                            employeePreDay.restDays = 0
                            employeePreDay.mornWorkingDays = 0
                            employeePreDay.nightWorkingDays = 0
                            employeePreDay.officeWorkingDays = 0
                        except ValueError:
                            pass
                elif(i == 0):
                    # 对姓名列的操作
                    employees.append(element)

                # 设置参数
                employee.workType = element
                employee.weekNum = weekDay
                employee.mornWorked = mornWorkedFlag
                employee.nightWorked = nightWorkedFlag
                employee.officeWorked = officeWorkedFlag
                employee.rested = restedFlag

                employee.isLastDayNight = 1 if employeePreDay is not None and employeePreDay.workType == "晚班" else 0
                if element == "早班":
                    employee.workingDays = employeePreDay.workingDays + \
                        1 if employeePreDay is not None else 1
                    employee.mornWorkingDays = employeePreDay.mornWorkingDays + \
                        1 if employeePreDay is not None else 1
                    mornWorkedFlag = 1

                elif element == "晚班":
                    employee.workingDays = employeePreDay.workingDays + \
                        1 if employeePreDay is not None else 1
                    employee.nightWorkingDays = employeePreDay.nightWorkingDays + \
                        1 if employeePreDay is not None else 1
                    nightWorkedFlag = 1

                elif element == "休":
                    employee.workingDays = 0
                    employee.restDays = employeePreDay.restDays + \
                        1 if employeePreDay is not None else 1
                    restedFlag = 1
                elif element == 1618 or element == "1618" or element == "大":
                    employee.workingDays = employeePreDay.workingDays + \
                        1 if employeePreDay is not None else 1
                    employee.officeWorkingDays = employeePreDay.officeWorkingDays + \
                        1 if employeePreDay is not None else 1
                    officeWorkedFlag = 1

                print(f"第{i}列 第{j}行：{array[j][i]}")
                print(employee.castToArray())
                if i == array.shape[1] - 1:
                    forNextPreEmployeeList.append(employee)


    # 将二维的employeeList转换为一维数组，并保存到对象数组中

    # 使用itertools.chain将二维数组转换为一维数组

    one_d_array = list(itertools.chain.from_iterable(row[1:] for row in employeeList[2:]))

    return one_d_array, forNextPreEmployeeList, employees

def writeResult(one_d_array, output_path):
    # 序列化对象数组到文件
    with open(output_path, 'wb') as f:
        pickle.dump(one_d_array, f)

    # 从文件反序列化对象数组
    with open(output_path, 'rb') as f:
        loaded_people = pickle.load(f)
    print("从文件中读取的对象：")
    index = 0
    for row in loaded_people:
        index += 1
        print(row.castToArray())
    for row in loaded_people:
        print(row.cast2OneHotData())
    print(index)

def load_excels2File(excels, output_path):
    result = []
    employeePreList = None
    employees = None
    for excel_path in excels:
        # TODO 不对，employees还没有确定
        loadedArray, employeePreList, employees = load_excel(excel_path, employeePreList, employees)
        result = np.concatenate((result, loadedArray))
    writeResult(result, output_path)

# load_excels2File(['testFebruary.xlsx', 'test.xlsx'], 'train_employee.pkl')
result, employeePreList, employees = load_excel('testFebruary.xlsx')
print("next:")
for row in employeePreList:
   print(row.castToArray())
for emp in employees:
    print(emp)

result2, ep2, eps2 = load_excel('test.xlsx',employeePreList,employees)
for row in result2:
    print(row.castToArray())

# 四
# 第28列 第2行：晚班
# [10, 3, 6, 6, 7, 1, 0, 0, 0, 0, 4]
# 第28列 第3行：早班
# [7, 2, 8, 10, 4, 0, 0, 1, 0, 0, 4]
# 第28列 第4行：大
# [10, 1, 10, 5, 4, 0, 1, 1, 0, 0, 4]
# 第28列 第5行：休
# [7, 0, 9, 8, 5, 0, 1, 1, 1, 0, 4]