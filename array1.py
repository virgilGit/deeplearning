# 两个字符串 abcd
str1 = 'abcefacq'
str2 = 'bcefbbac'
# 转换成两个字符数组 ['a', 'b', 'c', 'd']
array1 = [char for char in str1]
array2 = [char for char in str2]

# 记录最大值
max = 0

rows, cols = len(array1), len(array2)  # 指定二维数组的行数和列数  
value = 0  # 指定初始化值  
  
# 使用列表推导式初始化二维数组  
params = [[value for _ in range(cols)] for _ in range(rows)]  
  
for i, valuei in enumerate(array1):
    for j, valuej in enumerate(array2):
        if array1[i] == array2[j]:
            if i > 0 and j > 0:
                params[i][j] = params[i-1][j-1] + 1
            else:
                params[i][j] = 1
            if params[i][j] > max:
                max = params[i][j]
        else:
            params[i][j] = 0

for row in params:
    print(row)

# 同现矩阵
print(f'最大值是{max}')



# params ()

# 循环 for i in array1
#        for j in array2
#           if (array1[i] == array[j])
#              判断斜对角 i j > 0
#              array1[i - 1]