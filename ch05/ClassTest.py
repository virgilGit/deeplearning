import pickle
import os
class MyClass:
    i = 123
    def __init__(self, i=None):
        if i is None:
            i = 100
        else:
            self.i = i
    def printTest(self):
        print(self.i)

x = MyClass(i=999)
x2 = MyClass()

x.printTest()
x2.printTest()

with open('test.pkl', 'wb') as f:
    pickle.dump(x, f)

# with open('test.pkl', 'rb') as f:
#     loadx = pickle.load(f)
# print()
# print("loaded ...")
# print(loadx)
# print(loadx.printTest())

# python -m pip install pandas openpyxl 