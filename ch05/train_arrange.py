# coding: utf-8
import sys, os
sys.path.append(os.pardir)

import numpy as np
# from dataset.mnist import load_mnist
from two_layer_net import TwoLayerNet

# 读入数据
# 读入数据，从文件读取测试数据
# 每条 x_train 11; t_train 4
# 我得确定x_train
(x_train, t_train), (x_test, t_test) = load_mnist(normalize=True, one_hot_label=True)

print("加载shape...")
print(x_train.shape) # 60000 784
print(t_train.shape) # 60000 10
print(x_test.shape) # 10000 784
print(t_test.shape) # 10000 10

print("显示测试数据")
print(x_train[0])
print(x_train[0].shape)

network = TwoLayerNet(input_size=784, hidden_size=50, output_size=10)

iters_num = 10000
train_size = x_train.shape[0]
batch_size = 100
learning_rate = 0.1

train_loss_list = []
train_acc_list = []
test_acc_list = []

iter_per_epoch = max(train_size / batch_size, 1)

for i in range(iters_num):
    batch_mask = np.random.choice(train_size, batch_size)
    x_batch = x_train[batch_mask]
    t_batch = t_train[batch_mask]
    
    # 梯度
    #grad = network.numerical_gradient(x_batch, t_batch)
    grad = network.gradient(x_batch, t_batch)
    
    # 更新
    for key in ('W1', 'b1', 'W2', 'b2'):
        network.params[key] -= learning_rate * grad[key]
    
    loss = network.loss(x_batch, t_batch)
    train_loss_list.append(loss)
    
    
    if i % iter_per_epoch == 0:
        train_acc = network.accuracy(x_train, t_train)
        test_acc = network.accuracy(x_test, t_test)
        train_acc_list.append(train_acc)
        test_acc_list.append(test_acc)
        print(train_acc, test_acc)

# 导出 W1 b1 W2 b2 为文件
# 使用pickle能够实现序列化和反序列化

# 能够读取文件载入 W1 b1 W2 b2 并生成TwoLayerNet 进行判断 predict


